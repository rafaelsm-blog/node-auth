const records = [
  {
    id: '1',
    username: 'johndoe',
    name: 'John Doe',
    password: 'secret',
    email: 'johndoe@example.com',
  },
  {
    id: '2',
    username: 'janedoe',
    name: 'Jane Doe',
    password: 'birthday',
    email: 'janedoe@example.com',
  },
]

const findById = id => {
  for (let user of records) {
    if (user.id === id) {
      return user
    }
  }
}

const findByUsername = username => {
  for (let user of records) {
    if (user.username === username) {
      return user
    }
  }
}

module.exports = {
  findById,
  findByUsername,
}
