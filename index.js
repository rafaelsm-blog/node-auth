const express = require('express')
const bodyParser = require('body-parser')
const session = require('express-session')
const flash = require('express-flash')
const db = require('./db')

const app = express()

app.set('view engine', 'pug')
app.set('views', 'views')

app.use(bodyParser.urlencoded({ extended: false }))

app.use(
  session({
    secret: 'secretToEncryptSession',
    cookie: {
      maxAge: 15 * 60 * 1000,
    },
  })
)

app.use(flash())

app.get('/', (req, res) => {
  res.render('index', { user: db.findById('1') })
})

app.get('/profile', (req, res) => {
  res.render('profile', { user: db.findById('2') })
})

app.get('/login', (req, res) => {
  res.render('login')
})

app.get('/logout', (req, res) => {
  res.redirect('/login')
})

app.use((req, res, next) => {
  res.status(404).render('404')
})

const port = 5000
app.listen(port, () => {
  console.log(`listening on port ${port}`)
})
